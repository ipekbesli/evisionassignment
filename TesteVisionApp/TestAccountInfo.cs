﻿using ConsoleApplication1;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteVisionApp
{
    [TestFixture]
    class TestAccountInfo
    {
        [TestCase]
        public void FreshAmount()
        {
            var mock = new Mock<IAccountService>();

            mock.Setup(x => x.GetAccountAmount(1)).Returns(100);
            AccountInfo _accountInfo = new AccountInfo(1, mock.Object);
            _accountInfo.RefreshAmount();
            Assert.AreEqual(100, _accountInfo.Amount);
        }

    }

}
